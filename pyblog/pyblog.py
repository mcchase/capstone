#!/usr/bin/env python3
import requests
import json
from requests.auth import HTTPBasicAuth
import os
import argparse


def get_args(mode):
    if mode == 'unittest':
        action = None
        user_file = None
    else:
        parser = argparse.ArgumentParser(description="Usage pyblog.py [ read | upload -f <filename> ]")
        parser.add_argument('action')
        parser.add_argument('-f')
        args = parser.parse_args()
        user_file = args.f
        action = args.action
        if action == 'read':
            pass
        elif action == 'upload' and user_file is not None:
            pass
        else:
            raise Exception("Invalid action provided.  See pyblog.py -h for help")
    return action, user_file


#  Wordpress connectivity variables
def get_creds(mode):
    if mode == 'unittest':
        user = None
        passwd = None
        url = None
    else:
        user = os.getenv('WORDPRESS_USERNAME')
        passwd = os.getenv('WORDPRESS_PASSWORD')
        url = os.getenv('WORDPRESS_URL')
    return user, passwd, url


def get_last_blog(user, passwd, url):
    try:
        retrieve = requests.get(url, auth=HTTPBasicAuth(user, passwd), headers={"Accept": "application/json"})
    except:
        display_post = "Wordpress is not available"
        return display_post

    if retrieve.ok:
        post_data = retrieve.json()
        current_post = post_data[0]
        display_post = {
            "date": current_post.get("date"),
            "link": current_post.get("link"),
            "title": current_post.get("title").get("rendered"),
            "content": current_post.get("content").get("rendered")
        }
    elif retrieve.status_code == 401:
        display_post = "Unauthorized to access Wordpress"
        return display_post
    else:
        display_post = "Unable to access Wordpress"
        return display_post

    return display_post


def upload_blog_post(user, passwd, url, user_file):
    file_name = open(user_file)
    with file_name as f:
        title = f.readline()
        content = f.read()
    status = 'publish'
    post = {
        'title': title,
        'status': status,
        'content': content,
    }
    try:
        submit = requests.post(url, auth=HTTPBasicAuth(user, passwd), headers={"Accept": "application/json"}, json=post)
    except:
        post_message = "Wordpress is not available"
        return post_message

    if submit.ok:
        post_message = "Your post has been uploaded. " + json.loads(submit.content)['link']
    else:
        post_message = "There was a problem uploading your post."
    return post_message


def print_last_blog(display_post):
    if type(display_post) != str:
        print("Date: ", display_post["date"])
        print("Title: ", display_post["title"])
        print("Content:")
        print(display_post["content"])
        print("Link to blog post: ", display_post["link"])
    else:
        print(display_post)


if __name__ == "__main__":
    action, user_file = get_args('prod')
    user, passwd, url = get_creds('prod')
    if action == 'read':
        print_last_blog(get_last_blog(user, passwd, url))
    if action == 'upload':
        print(upload_blog_post(user, passwd, url, user_file))
